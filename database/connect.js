const mysql = require('mysql');
const dbconfig = require('./database.json')

const sesion = {
  host: dbconfig.host,
  user: dbconfig.database_user,
  password: dbconfig.database_password,
  database: dbconfig.database,
  port: dbconfig.database_port,
  multipleStatements: true
}

const db = mysql.createConnection(sesion);

db.connect(function(err){
  if(err)
    throw err;
  console.log('DB connect'); 
});

module.exports = db;

// Este es el ejemplo de una consulta
// router.get('/estados', function(req, res){
//   con.query("SELECT DISTINCT estado FROM ubicacion", function (err, result, fields) {
//     if (err) throw err;
//     console.log(result);
//     res.send(result);
//   });
// });