const db = require('../database/connect')
var hidrantesCtrl = {};

hidrantesCtrl.getHidrantes = function(req, res){
  var query_string = `SELECT * FROM hidrantes INNER JOIN parroquias ON parroquias.id_parroquia = hidrantes.parroquia`

	db.query( query_string  , function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		} 

		res.status(200);
		res.send(result);

	});
}

hidrantesCtrl.getOneHidrantes = function(req, res){
  var query_string = `SELECT * FROM hidrantes JOIN parroquias ON parroquias.id_parroquia = hidrantes.parroquia WHERE id_hidrante = ?`

	db.query( query_string  ,  [req.params.hidrante_id], function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		} 

		if( result.length == 0 ){
			res.status(404);
			res.send( `The fire hydrant with id = ${req.params.hidrante_id} could not be found.`);
		}

		res.status(200);
		res.send(result);

	});
}

hidrantesCtrl.updateHidrante = function(req, res){
  console.log(req.body)

	var query_string = `UPDATE hidrantes SET longitud= ? , latitud= ? , direccion = ?, parroquia = ? , ud= ? , referencia = ?, tipo = ?   WHERE id_hidrante = ? `
	var string_params = [ req.body.longitud, req.body.latitud , req.body.direccion, req.body.parroquia, req.body.ud , req.body.referencia , req.body.tipo , req.params.hidrante_id ]
	
	db.query( query_string , string_params , function (err, result, fields) {

		if (err){

			if ( err.errno == "1048" || err.code =="ER_BAD_NULL_ERROR" ){
				res.status(400);
				res.send("The request body is not correctly formed.");
			}

			res.status(400);
			res.send(err);
		} 

		//si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
		//es decir, es un 404
		if ( result.affectedRows == 0){
			res.status(404);
			res.send( `The fire hydrant with id = ${req.params.hidrante_id} could not be found.`);
		}

		res.status(200);

		response = { message: "hydrant updated successfuly!",
					 hidrante_id : req.params.hidrante_id , 
					 longitud : req.body.longitud, 
					 latitud: req.body.latitud , 
					 direccion : req.body.direccion,
					 parroquia: req.body.parroquia, 
					 ud: req.body.ud , 
					 referencia: req.body.referencia , 
					 tipo: req.body.tipo 
					}

		res.send(response);

	});
}

hidrantesCtrl.deleteHidrante = function(req, res){
  var query_string = `DELETE FROM hidrantes WHERE id_hidrante = ?`

	db.query( query_string , [req.params.hidrante_id], function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		} 

		//si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
		//es decir, es un 404
		if ( result.affectedRows == 0){
			res.status(404);
			res.send( `The fire hydrant with id = ${req.params.hidrante_id} could not be found.`);
		}

		res.status(200);
		response = { message: "fire hydrant deleted successfuly!", hidrante_id : req.params.hidrante_id }
		res.send(response);

	});	
}

module.exports = hidrantesCtrl;