const db = require('../database/connect')
var responsablesCtrl = {};

//--responsables
    //TODO create responsable
    //TODO edit responsable
    //TODO deletes responsables
    //TODO obtener responsable de una inspeccion

responsablesCtrl.getResponsableByInspeccion = function(req, res){
  var query_string = `SELECT * FROM responsables_por_inspeccion WHERE id_inspeccion = ?`

	db.query( query_string  ,  [req.params.id_inspeccion], function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		}

		if( result.length == 0 ){
			res.status(404);
			res.send( `The responsible for the inspection with id = ${req.params.id_inspeccion} could not be found.`);
		}

		res.status(200);
		res.send(result);

	});
}

responsablesCtrl.createResponsable = function(req, res){
  console.log(req.body)

  var data = {
    id_inspeccion: req.body.id_inspeccion,
    nombre_inspector: req.body.nombre_inspector,
    correo_inspector: req.body.correo_inspector,
    telefono_inspector: req.body.telefono_inspector,
    dependencia: req.body.dependencia
  }

  var query_string = `INSERT INTO responsables_por_inspeccion (id_inspeccion, nombre_inspector, correo_inspector, 
    telefono_inspector, dependencia) values ( ? , ? , ?, ?, ?)`

  db.query( query_string , [data.id_inspeccion, data.nombre_inspector, data.correo_inspector, data.telefono_inspector, 
    data.dependencia], function (err, result, fields) {

		if (err){

      if ( err.errno == "1048" || err.code =="ER_BAD_NULL_ERROR" ){
				res.status(400);
				res.send("The request body is not correctly formed.");
			}

			res.status(400);
			res.send(err);
		} 

		res.status(201);
		response = {  message: "responsible created successfuly!", responsable: data}
		res.send(response);

	});
}

responsablesCtrl.updateResponsable = function(req, res){
  console.log(req.body)

  var data = {
    nombre_inspector: req.body.nombre_inspector,
    correo_inspector: req.body.correo_inspector,
    telefono_inspector: req.body.telefono_inspector,
    dependencia: req.body.dependencia
  }

  var query_string = `UPDATE responsables_por_inspeccion SET nombre_inspector= ? , correo_inspector= ? , telefono_inspector = ?, 
  dependencia = ? WHERE id_inspeccion = ? `
	var string_params = [data.nombre_inspector, data.correo_inspector, data.telefono_inspector, data.dependencia, req.body.id_inspeccion]
	
	db.query( query_string , string_params , function (err, result, fields) {

		if (err){

			if ( err.errno == "1048" || err.code =="ER_BAD_NULL_ERROR" ){
				res.status(400);
				res.send("The request body is not correctly formed.");
			}

			res.status(400);
			res.send(err);
		} 

		//si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
		//es decir, es un 404
		if ( result.affectedRows == 0){
			res.status(404);
			res.send( `The responsible for the inspection with id = ${req.params.id_inspeccion} could not be found.`);
		}

		res.status(200);

    data.id_inspeccion = req.params.id_inspeccion;
		response = { message: "responsible updated successfuly!", responsable: data}

		res.send(response);

	});
}

responsablesCtrl.deleteResponsable = function(req, res){
  var query_string = `DELETE FROM responsables_por_inspeccion WHERE id_inspeccion = ?`

	db.query( query_string , [req.params.id_inspeccion], function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		} 

		//si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
		//es decir, es un 404
		if ( result.affectedRows == 0){
			res.status(404);
			res.send( `The responsible for the inspection with id = ${req.params.id_inspeccion} could not be found.`);
		}

		res.status(204);
		res.end();
	});
}

module.exports = responsablesCtrl;