const db = require('../database/connect')
var inspeccionesCtrl = {};

var getDistanceFromLatLonInKm = function (lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = (R * c) * 1000; // Distance in m
  return d;
}

var deg2rad = function (deg) {
  return deg * (Math.PI/180)
}


var createHidrante = function (hidrante) {
  var query_string = `INSERT INTO hidrantes (longitud, latitud, direccion, parroquia, UD, referencia, tipo) values 
  ( ? , ? , ?, ?, ?, ?, ?)`

  db.query(query_string, [hidrante.longitud, hidrante.latitud, hidrante.direccion, hidrante.parroquia, hidrante.ud,
  hidrante.referencia, hidrante.tipo], function (err, result, fields) {

    if (err) {
      return err;
    }

    return result.insertId;
  });
}


var verificarUsuario = function (id) {
  var query_string = `SELECT * FROM usuarios WHERE id_usuario = ?`

  db.query(query_string, [id], function (err, result, fields) {

    if (err) {
      return false;
    }

    if (result.length == 0) {
      console.log("not here");
      return false;

    }

    return true;
  });
}

inspeccionesCtrl.getInspeccionHidrante = function (req, res) {

  var query_string = `SELECT inspecciones_hidrantes.* FROM inspecciones_hidrantes  JOIN estatus ON estatus.id_estatus = inspecciones_hidrantes.estatus WHERE inspecciones_hidrantes.id_inspeccion = ? `
  var query_params = [req.params.id_inspeccion]


  db.query(query_string, [query_params], function (err, result, fields) {

    if (err) {
      res.status(400);
      res.send(err);
    }

    if (result.length == 0) {
      res.status(404);
      res.send(`The inspection with id = ${req.params.id_inspeccion} could not be found.`);
    }

    res.status(200);
    res.send(result);

  });
}

inspeccionesCtrl.getInspeccionesHidrante = function (req, res) {
  var query_string = `SELECT inspecciones_hidrantes.* FROM inspecciones_hidrantes JOIN estatus ON estatus.id_estatus = inspecciones_hidrantes.estatus INNER JOIN hidrantes ON hidrantes.id_hidrante = ?
  AND inspecciones_hidrantes.id_hidrante = ? `

  db.query(query_string, [req.params.id_hidrante, req.params.id_hidrante], function (err, result, fields) {

    if (err) {
      res.status(400);
      res.send(err);
    }

    if (result.length == 0) {
      res.status(404);
      res.send(`The fire hydrant with id = ${req.params.hidrante_id} could not be found.`);
    }

    res.status(200);
    res.send(result);

  });
}

inspeccionesCtrl.getInspecciones = function (req, res) {
  var query_string = `SELECT * FROM ( SELECT inspecciones_hidrantes.id_hidrante,  MAX(inspecciones_hidrantes.fecha) as fecha
                      FROM inspecciones_hidrantes  GROUP BY inspecciones_hidrantes.id_hidrante )  h 
                      INNER JOIN inspecciones_hidrantes ON inspecciones_hidrantes.id_hidrante = h.id_hidrante
                                  AND inspecciones_hidrantes.fecha = h.fecha
                      INNER JOIN hidrantes ON h.id_hidrante = hidrantes.id_hidrante 
                      INNER JOIN parroquias ON parroquias.id_parroquia = hidrantes.parroquia 
                      INNER JOIN estatus ON estatus.id_estatus = inspecciones_hidrantes.estatus  `
  var query_params = []

  if (req.query.municipio) {
    query_string += 'AND hidrantes.municipio = ?'
    query_params.push(req.query.municipio)
  }

  if (req.query.parroquia) {
    query_string += 'AND hidrantes.parroquia = ?'
    query_params.push(req.query.parroquia)
  }

  if (req.query.referencia) {
    query_string += 'AND hidrantes.referencia = ?'
    query_params.push(req.query.referencia)
  }

  db.query(query_string, [query_params], function (err, result, fields) {

    if (err) {
      res.status(400);
      res.send(err);
    }

    if (result.length == 0) {
      res.status(404);
      res.send(`The fire hydrant have not inspections`);
    }

    res.status(200);
    res.send(result);

  });
}

inspeccionesCtrl.getEstadisticasEstatus = function (req, res) {
  var query_string = `SELECT parroquias.parroquia, parroquias.id_parroquia, COUNT(inspecciones_hidrantes.id_hidrante) as total, SUM(inspecciones_hidrantes.estatus = 1) as h_buenos, SUM(inspecciones_hidrantes.estatus = 2) as h_malos, SUM(inspecciones_hidrantes.estatus = 3) as h_regulares
                      FROM ( SELECT inspecciones_hidrantes.id_hidrante,  MAX(inspecciones_hidrantes.fecha) as fecha
                        FROM inspecciones_hidrantes  GROUP BY inspecciones_hidrantes.id_hidrante )  h 
                        INNER JOIN inspecciones_hidrantes ON inspecciones_hidrantes.id_hidrante = h.id_hidrante
                                    AND inspecciones_hidrantes.fecha = h.fecha
                        INNER JOIN hidrantes ON h.id_hidrante = hidrantes.id_hidrante 
                        INNER JOIN parroquias ON parroquias.id_parroquia = hidrantes.parroquia 
                        INNER JOIN estatus ON estatus.id_estatus = inspecciones_hidrantes.estatus  
                      GROUP BY parroquias.id_parroquia, parroquias.parroquia`

  var query_params = []

  db.query(query_string, [query_params], function (err, result, fields) {

    if (err) {
      res.status(400);
      res.send(err);
    }

    if (result.length == 0) {
      res.status(404);
      res.send(`The fire hydrant have not inspections`);
    }

    res.status(200);
    res.send(result);

  });
}


inspeccionesCtrl.getEstadisticasPartes = function (req, res) {
  var query_string = `SELECT parroquias.parroquia, parroquias.id_parroquia, 
                      COUNT(inspecciones_hidrantes.id_hidrante) as total, 
                      SUM(inspecciones_hidrantes.cantidad_tapas = 0) as sin_tapas, 
                      SUM(inspecciones_hidrantes.cantidad_dados = 0) as sin_dados, 
                      SUM(inspecciones_hidrantes.cantidad_cadenas = 0) as sin_cadenas,
                      SUM(inspecciones_hidrantes.cantidad_vabulas = 0) as sin_valvulas
                      FROM ( SELECT inspecciones_hidrantes.id_hidrante,  MAX(inspecciones_hidrantes.fecha) as fecha
                                      FROM inspecciones_hidrantes  GROUP BY inspecciones_hidrantes.id_hidrante )  h 
                                      INNER JOIN inspecciones_hidrantes ON inspecciones_hidrantes.id_hidrante = h.id_hidrante
                                                  AND inspecciones_hidrantes.fecha = h.fecha
                                      INNER JOIN hidrantes ON h.id_hidrante = hidrantes.id_hidrante 
                                      INNER JOIN parroquias ON parroquias.id_parroquia = hidrantes.parroquia 
                                      INNER JOIN estatus ON estatus.id_estatus = inspecciones_hidrantes.estatus  
                    GROUP BY parroquias.id_parroquia, parroquias.parroquia`
  var query_params = []

  db.query(query_string, [query_params], function (err, result, fields) {

    if (err) {
      res.status(400);
      res.send(err);
    }

    if (result.length == 0) {
      res.status(404);
      res.send(`The fire hydrant have not inspections`);
    }

    res.status(200);
    res.send(result);

  });
}

inspeccionesCtrl.getEstadisticasOperatividad = function (req, res) {
  var query_string = `SELECT parroquias.parroquia, parroquias.id_parroquia, 
                      COUNT(inspecciones_hidrantes.id_hidrante) as total, 
                      SUM(inspecciones_hidrantes.condicion_llave_auxiliar = 'NO OPERATIVA') as llaves_auxiliares_no_operativas, 
                      SUM(inspecciones_hidrantes.operatividad_llave_op = 'NO OPERATIVA') as llaves_operativas_no_operativas
                      FROM ( SELECT inspecciones_hidrantes.id_hidrante,  MAX(inspecciones_hidrantes.fecha) as fecha
                                      FROM inspecciones_hidrantes  GROUP BY inspecciones_hidrantes.id_hidrante )  h 
                                      INNER JOIN inspecciones_hidrantes ON inspecciones_hidrantes.id_hidrante = h.id_hidrante
                                                  AND inspecciones_hidrantes.fecha = h.fecha
                                      INNER JOIN hidrantes ON h.id_hidrante = hidrantes.id_hidrante 
                                      INNER JOIN parroquias ON parroquias.id_parroquia = hidrantes.parroquia 
                                      INNER JOIN estatus ON estatus.id_estatus = inspecciones_hidrantes.estatus  
                    GROUP BY parroquias.id_parroquia, parroquias.parroquia  `
  var query_params = []

  db.query(query_string, [query_params], function (err, result, fields) {

    if (err) {
      res.status(400);
      res.send(err);
    }

    if (result.length == 0) {
      res.status(404);
      res.send(`The fire hydrant have not inspections`);
    }

    res.status(200);
    res.send(result);

  });
}

var verificarHidranteID = function (id) {
  var query_string = `SELECT * FROM hidrantes WHERE id_hidrante = ?`

  db.query(query_string, [id], function (err, result, fields) {

    if (err) {
      return false;
    }

    if (result.length == 0) {
      return false;
    }

    return true;
  });
}


var verificarHidrante = async function (hidrante) {
  var query_string = `SELECT * FROM hidrantes WHERE direccion = ? AND parroquia = ? AND UD = ? AND referencia = ? AND tipo = ?`

  db.query(query_string, [hidrante.direccion, hidrante.parroquia, hidrante.ud,
  hidrante.referencia, hidrante.tipo], function (err, result, fields) {

    if (err) {
      return null;
    }

    if (result.length == 0) {
      return null;
    }

    console.log(result.length)

    for(item of result){

      if(getDistanceFromLatLonInKm(hidrante.latitud, hidrante.longitud, item.latitud, item.longitud) <= 200)
        return item.id_hidrante

    }

    return null;
  });
}

inspeccionesCtrl.createInspeccion = async function (req, res) {
  console.log(req.body)

  var data = {
    fecha: req.body.fecha,
    hora: req.body.hora,
    color: req.body.color,
    cantidad_dados: req.body.cantidad_dados,
    cantidad_tapas: req.body.cantidad_tapas,
    cantidad_cadenas: req.body.cantidad_cadenas,
    cantidad_vabulas: req.body.cantidad_vabulas,
    condicion_llave_auxiliar: req.body.condicion_llave_auxiliar,
    distancia_llave_auxiliar: req.body.distancia_llave_auxiliar,
    operatividad_llave_op: req.body.operatividad_llave_op,
    filtraciones: req.body.filtraciones,
    posee_agua: req.body.posee_agua,
    observaciones: req.body.observaciones,
    id_usuario: req.body.id_usuario,
    estatus: req.body.estatus,

    //hidrante
    longitud: req.body.longitud,
    latitud: req.body.latitud,
    direccion: req.body.direccion,
    parroquia: req.body.parroquia,
    ud: req.body.ud,
    referencia: req.body.referencia,
    tipo: req.body.tipo
    
  }

  var query_string = `SELECT * FROM hidrantes WHERE direccion = ? AND parroquia = ? AND UD = ? AND referencia = ? AND tipo = ?`
  
  db.query(query_string, [data.direccion, data.parroquia, data.ud, data.referencia, data.tipo], function (err, result, fields) {
    
    if (err) {
      res.status(400);
      console.log("here")
      res.send(err);
      return null;;
    }

    var exists = false;
    var id_hidrante = null;

    for(item of result){

      if(getDistanceFromLatLonInKm(data.latitud, data.longitud, item.latitud, item.longitud) <= 200){
          //not create hidrante
          exists = true;
          id_hidrante = item.id_hidrante;
          break;
      }

    }

    if( !exists ){

        var query_string = `INSERT INTO hidrantes (longitud, latitud, direccion, parroquia, UD, referencia, tipo ) values ( ? , ?, ?, ?, ?, ?, ?)`
    
        db.query(query_string, [data.longitud, data.latitud, data.direccion, data.parroquia, data.ud, data.referencia, data.tipo], function (err, result, fields) {
    
            if (err) {
      
              if (err.errno == "1048" || err.code == "ER_BAD_NULL_ERROR") {
                res.status(400);
                res.send("The request body is not correctly formed.");
              }
      
              res.status(400);
			        res.send(err);
			  
		    	}
			
		      	id_hidrante = result.insertId;

            var query_string = `INSERT INTO inspecciones_hidrantes (fecha, hora, color, cantidad_dados, cantidad_tapas, cantidad_cadenas,`+ 
              `cantidad_vabulas, condicion_llave_auxiliar, distancia_llave_auxiliar, operatividad_llave_op, filtraciones, posee_agua,`+
              `observaciones, id_usuario, estatus, id_hidrante) values ( ? , ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,?, ?, ?, ?)`
        
            db.query(query_string, [data.fecha, data.hora, data.color, data.cantidad_dados, data.cantidad_tapas, data.cantidad_cadenas,
            data.cantidad_vabulas, data.condicion_llave_auxiliar, data.distancia_llave_auxiliar, data.operatividad_llave_op, data.filtraciones,
            data.posee_agua, data.observaciones, data.id_usuario, data.estatus, id_hidrante], function (err, result, fields) {
        
              if (err) {
        
                if (err.errno == "1048" || err.code == "ER_BAD_NULL_ERROR") {
                  res.status(400);
                  res.send("The request body is not correctly formed.");
                }
        
                res.status(400);
                res.send(err);
              }
        
              data.id_inspeccion = result.insertId;
              res.status(201);
              response = { message: "inspection created successfuly!", inspeccion: data }
              res.send(response);
        
            });
            
          });


    }else{

		var query_string = `INSERT INTO inspecciones_hidrantes (fecha, hora, color, cantidad_dados, cantidad_tapas, cantidad_cadenas, 
			cantidad_vabulas, condicion_llave_auxiliar, distancia_llave_auxiliar, operatividad_llave_op, filtraciones, posee_agua, 
			observaciones, id_usuario, estatus, id_hidrante) values ( ? , ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,?, ?, ?, ?)`

		db.query(query_string, [data.fecha, data.hora, data.color, data.cantidad_dados, data.cantidad_tapas, data.cantidad_cadenas,
		data.cantidad_vabulas, data.condicion_llave_auxiliar, data.distancia_llave_auxiliar, data.operatividad_llave_op, data.filtraciones,
		data.posee_agua, data.observaciones, data.id_usuario, data.estatus, id_hidrante], function (err, result, fields) {

			if (err) {

			if (err.errno == "1048" || err.code == "ER_BAD_NULL_ERROR") {
				res.status(400);
				res.send("The request body is not correctly formed.");
			}

			res.status(400);
			res.send(err);
			}

			data.id_inspeccion = result.insertId;
			res.status(201);
			response = { message: "inspection created successfuly!", inspeccion: data }
			res.send(response);

		});

  	} 

		
})
 

}

inspeccionesCtrl.updateInspeccion = async function (req, res) {
  console.log(req.body)

  var data = {
    fecha: req.body.fecha,
    hora: req.body.hora,
    color: req.body.color,
    cantidad_dados: req.body.cantidad_dados,
    cantidad_tapas: req.body.cantidad_tapas,
    cantidad_cadenas: req.body.cantidad_cadenas,
    cantidad_vabulas: req.body.cantidad_vabulas,
    condicion_llave_auxiliar: req.body.condicion_llave_auxiliar,
    distancia_llave_auxiliar: req.body.distancia_llave_auxiliar,
    operatividad_llave_op: req.body.operatividad_llave_op,
    filtraciones: req.body.filtraciones,
    posee_agua: req.body.posee_agua,
    observaciones: req.body.observaciones,
    id_usuario: req.body.id_usuario,
    id_hidrante: req.body.id_hidrante,
    estatus: req.body.estatus
  }

  //validar usuario
  if (!await verificarUsuario(data.id_usuario)) {
    res.status(404);
    res.send(`User with id = ${data.id_usuario} could not be found.`);
  }

  //validar hidrante
  if (!await verificarHidranteID(data.id_hidrante)) {
    res.status(404);
    res.send(`The hydrant with id = ${data.id_hidrante} could not be found.`);
  }

  var query_string = `UPDATE inspecciones_hidrantes SET fecha= ? , hora= ? , color= ?, cantidad_dados= ?, cantidad_tapas= ?
  cantidad_cadenas = ?, cantidad_vabulas= ?, condicion_llave_auxiliar= ?, distancia_llave_auxiliar= ?, operatividad_llave_op= ?, 
  filtraciones= ?, posee_agua= ?, observaciones= ?, id_usuario= ?, estatus=?, id_hidrante= ? WHERE id_inspeccion = ? `
  var string_params = [data.fecha, data.hora, data.color, data.cantidad_dados, data.cantidad_tapas, data.cantidad_cadenas,
  data.cantidad_vabulas, data.condicion_llave_auxiliar, data.distancia_llave_auxiliar, data.operatividad_llave_op, data.filtraciones,
  data.posee_agua, data.observaciones, data.id_usuario, data.estatus, data.id_hidrante, req.params.id_inspeccion]

  db.query(query_string, string_params, function (err, result, fields) {

    if (err) {

      if (err.errno == "1048" || err.code == "ER_BAD_NULL_ERROR") {
        res.status(400);
        res.send("The request body is not correctly formed.");
      }

      res.status(400);
      res.send(err);
    }

    //si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
    //es decir, es un 404
    if (result.affectedRows == 0) {
      res.status(404);
      res.send(`The inspection with id = ${req.params.id_inspeccion} could not be found.`);
    }

    res.status(200);

    data.id_inspeccion = req.params.id_inspeccion;
    response = {
      message: "inspection updated successfuly!",
      responsable: data
    }

    res.send(response);

  });
}

inspeccionesCtrl.deleteInspeccion = function (req, res) {
  var query_string = `DELETE FROM inspecciones_hidrantes WHERE id_inspeccion = ?`

  db.query(query_string, [req.params.id_inspeccion], function (err, result, fields) {

    if (err) {
      res.status(400);
      res.send(err);
    }

    //si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
    //es decir, es un 404
    if (result.affectedRows == 0) {
      res.status(404);
      res.send(`The inspection with id = ${req.params.id_inspeccion} could not be found.`);
    }

    res.status(204);
    res.end();
  });
}

module.exports = inspeccionesCtrl;