const db = require('../database/connect')
const bcrypt = require('bcrypt');
var usuariosCtrl = {};

usuariosCtrl.getOneUsuarios = function(req, res){
  var query_string = `SELECT * FROM usuarios WHERE id_usuario = ?`

	db.query( query_string  ,  [req.params.user_id], function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		} 

		if( result.length == 0 ){
			res.status(404);
			res.send( `The user with id = ${req.params.user_id} could not be found.`);
		}

		res.status(200);
		res.send(result);

	});	
}

usuariosCtrl.getUsuarios = function(req, res){
  var query_string = `SELECT * FROM usuarios `

	db.query( query_string  , function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		} 

		res.status(200);
		res.send(result);

	});
}

usuariosCtrl.createUsuarios = function(req, res){

  console.log(req.body)

	hashed_password = bcrypt.hashSync(req.body.password, saltRounds = 10);


	var query_string = `INSERT INTO usuarios (correo, password, id_creador) values ( ? , ? , ?)`

	db.query( query_string , [req.body.correo, hashed_password , req.body.id_creador], function (err, result, fields) {

		if (err){

			if(err.errno == "1062"  || err.code =="ER_DUP_ENTRY" ){
				res.status(409);
				res.send("The email already exists in the DB!");
			}
			else if ( err.errno == "1048" || err.code =="ER_BAD_NULL_ERROR" ){
				res.status(400);
				res.send("The request body is not correctly formed.");
			}

			res.status(400);
			res.send(err);
		} 

		res.status(201);
		response = {  message: "user created successfuly!", user_id : result.insertId ,  correo : req.body.correo, id_creador : req.body.id_creador }
		res.send(response);

	});
}

usuariosCtrl.updateUsuario = function(req, res){

  console.log(req.body)

	hashed_password = bcrypt.hashSync(req.body.password, saltRounds = 10);

	var query_string = `UPDATE usuarios SET correo= ? , password= ? , id_creador = ? WHERE id_usuario = ? `

	db.query( query_string , [req.body.correo, hashed_password , req.body.id_creador,  req.params.user_id ], function (err, result, fields) {

		if (err){

			if(err.errno == "1062"  || err.code =="ER_DUP_ENTRY" ){
				res.status(409);
				res.send("The email already exists in the DB!");
			}
			else if ( err.errno == "1048" || err.code =="ER_BAD_NULL_ERROR" ){
				res.status(400);
				res.send("The request body is not correctly formed.");
			}

			res.status(400);
			res.send(err);
		} 

		//si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
		//es decir, es un 404
		if ( result.affectedRows == 0){
			res.status(404);
			res.send( `The user with id = ${req.params.user_id} could not be found.`);
		}

		res.status(200);
		response = { message: "user updated successfuly!", user_id : req.params.user_id ,  correo : req.body.correo, id_creador : req.body.id_creador }
		res.send(response);

	});
}

usuariosCtrl.deleteUsuario = function(req, res){
  var query_string = `DELETE FROM usuarios WHERE id_usuario = ?`

	db.query( query_string , [req.params.user_id], function (err, result, fields) {

		if (err){
			res.status(400);
			res.send(err);
		} 

		//si ninguna columna fue afectada, significa que el id no se encuentra en la base de datos
		//es decir, es un 404
		if ( result.affectedRows == 0){
			res.status(404);
			res.send( `The user with id = ${req.params.user_id} could not be found.`);
		}

		res.status(200);
		response = { message: "user deleted successfuly!", user_id : req.params.user_id }
		res.send(response);

	});
}

module.exports = usuariosCtrl;