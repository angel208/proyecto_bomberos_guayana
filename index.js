const express = require('express')
const app = express()

const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcrypt');
const compression = require('compression');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const config = require("./config.json")
const db = require('./database/connect')
const inputvalidator = require('./inputvalidator.js')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('port', process.env.PORT || 3000);
app.use(express.json());
app.use(compression());
app.use(morgan('dev'));
app.set('JwtSecret', config.secret);

//guard middleware
var authorize = module.exports = function(req, res, next) {

    var token =  req.headers["x-access-token"] ;
   
    if (token) {

      jwt.verify(token, app.get('JwtSecret') , function(err, decoded) {
         
        if (err) {
            console.error("JWT Verification Error", err);
            return res.status(403).send(err);
         } else {
			//the token is correct
			req.decoded = decoded;
            return next();
         }

      });

    } 
    else {
    	res.status(403).send("Token not provided");
    }
}  

app.use('/usuarios', require('./routes/usuarios.routes'));
app.use('/hidrantes', require('./routes/hidrantes.routes'));
app.use('/inspecciones', require('./routes/inspecciones.routes'));
app.use('/responsables', require('./routes/responsables.routes'));
app.use('/parroquias', require('./routes/parroquias.routes'));

// -- LOGIN
app.post('/login', function(req, res) {
	
	db.query(`SELECT correo, password, id_usuario FROM usuarios WHERE correo = "${req.body.correo}"`, function (err, query_result, fields) {
		
		if (err) throw err;
		
		user = query_result[0];
		passwordAttempt = req.body.password;

		if (!user) {
			res.json({ success: false, message: 'Authentication failed. User not found.' });
		} 
		else if (user) {

			if(!bcrypt.compareSync( passwordAttempt , user.password )) {
				res.json({ success: false, message: 'Authentication failed. Wrong password.' });
			}
			else {

				// if user is found and password is right
				// create a token with only our given payload
				// we don't want to pass in the entire user since that has the password
				const payload = {
					username: user.correo     
				};

				var token = jwt.sign(payload, app.get('JwtSecret'), {
					expiresIn: '1440m' // 24 hours
				});

				// return the information including token as JSON
				res.json({
					success: true,
					token: token
				});
			} 
		}
		
	});

});

app.get('/estatus', function(req, res) {

	var query_string = `SELECT * FROM estatus`
  
	  db.query( query_string  , function (err, result, fields) {
  
		  if (err){
			  res.status(400);
			  res.send(err);
		  } 
  
		  res.status(200);
		  res.send(result);
  
	  });
});

app.listen( config.port , () => console.log(`Server listening on port ${config.port}!`))
