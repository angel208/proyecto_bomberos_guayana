var express = require('express');
var router = express.Router();
var authorize = require('../index');

var inspeccionesCtrl = require('../controllers/inspecciones.controller');

router.get('/', inspeccionesCtrl.getInspecciones)
router.get('/all/:id_hidrante', inspeccionesCtrl.getInspeccionesHidrante)
router.get('/:id_inspeccion', inspeccionesCtrl.getInspeccionHidrante)
router.post('/', authorize, inspeccionesCtrl.createInspeccion)
router.put('/:id_inspeccion', authorize, inspeccionesCtrl.updateInspeccion)
router.delete('/:id_inspeccion', authorize, inspeccionesCtrl.deleteInspeccion)

router.get('/estadisticas/estatus', inspeccionesCtrl.getEstadisticasEstatus)
router.get('/estadisticas/partes', inspeccionesCtrl.getEstadisticasPartes)
router.get('/estadisticas/operatividad', inspeccionesCtrl.getEstadisticasOperatividad)

module.exports = router;