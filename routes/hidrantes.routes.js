var express = require('express');
var router = express.Router();
var authorize = require('../index');

console.log(authorize)

var hidrantesCtrl = require('../controllers/hidrantes.controller');

router.get('/', hidrantesCtrl.getHidrantes)
router.get('/:hidrante_id', hidrantesCtrl.getOneHidrantes)
router.put('/:hidrante_id', authorize, hidrantesCtrl.updateHidrante)
router.delete('/:hidrante_id', authorize, hidrantesCtrl.deleteHidrante)

module.exports = router;