var express = require('express');
var router = express.Router();
var authorize = require('../index');

var usuariosCtrl = require('../controllers/usuarios.controller');


router.get('/', authorize, usuariosCtrl.getUsuarios )
router.get('/:user_id', authorize,  usuariosCtrl.getOneUsuarios)
router.post('/', authorize, usuariosCtrl.createUsuarios)
router.put('/:user_id', authorize, usuariosCtrl.updateUsuario)
router.delete('/:user_id', authorize, usuariosCtrl.deleteUsuario)

module.exports = router;