var express = require('express');
var router = express.Router();
var authorize = require('../index');

var responsablesCtrl = require('../controllers/responsables.controller');

router.get('/:id_inspeccion', responsablesCtrl.getResponsableByInspeccion)
router.post('/', authorize, responsablesCtrl.createResponsable)
router.put('/:id_inspeccion', authorize, responsablesCtrl.updateResponsable)
router.delete('/:id_inspeccion', authorize, responsablesCtrl.deleteResponsable)

module.exports = router;