input_validator = function ( tipo_texto  , string  ){

    var expresiones = {
        "numero" : "^[0-9]+$",
        "latlong" : "^[-]?[0-9.]+$",
        "numero_separador" : "^[-]?[0-9,.]+$",
        "cedula" : "^[A-Z-a-z]?[0-9]+$",
        "texto" : "^[A-Za-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚñÑ]+[A-Za-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚñÑ .,\\-\\s]*$",
        "palabras" : "^[A-Za-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚñÑ]+[A-Za-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝñÑü \\s]*$",
    }

    if (!(tipo_texto in expresiones)){
        return  false
    }

    if ( !(typeof string === 'string' || string instanceof String) ){
        return  false
    }

    var re = new RegExp( expresiones[tipo_texto] );

    if (re.test(string)) {
        return true
    } else {
        return false
    }
}

module.exports = input_validator;

//este es un ejemplo de como usar este validador en el archivo index.js
//const inputvalidator = require('./inputvalidator.js')
//console.log( inputvalidator( "latlong", "-1.32s" ) )